/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class CheckboxCondition extends ConditionBlock<{
    readonly checked: boolean;
}> {
    @condition
    isChecked(): boolean {
        const checkboxSlot = this.valueOf<boolean>();

        return (
            (checkboxSlot && checkboxSlot.value === this.props.checked) || false
        );
    }
}
