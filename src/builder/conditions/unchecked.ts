/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionBlock, pgettext, tripetto } from "@tripetto/builder";

/** Assets */
import ICON_UNCHECKED from "../../../assets/unchecked.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:unchecked`,
    version: PACKAGE_VERSION,
    icon: ICON_UNCHECKED,
    get label() {
        return pgettext("block:checkboxes", "All checkboxes unchecked");
    },
})
export class UncheckedCondition extends ConditionBlock {}
