/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    Markdown,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    isBoolean,
    isNumber,
    isString,
    npgettext,
    pgettext,
    slots,
    tripetto,
} from "@tripetto/builder";
import { Checkbox } from "./checkbox";
import { CheckboxCondition } from "./conditions/checkbox";
import { UncheckedCondition } from "./conditions/unchecked";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";
import { CounterCondition } from "./conditions/counter";
import { TCounterModes } from "../runner/conditions/counter";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_CHECKED from "../../assets/checked.svg";
import ICON_COUNTER from "../../assets/counter.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:checkboxes", "Checkboxes");
    },
})
export class Checkboxes extends NodeBlock {
    @definition("items")
    @affects("#name")
    readonly checkboxes = Collection.of<Checkbox, Checkboxes>(Checkbox, this);

    @definition("boolean", "optional")
    @affects("#required")
    @affects("#collection", "checkboxes")
    required?: boolean;

    @definition("number", "optional")
    min?: number;

    @definition("number", "optional")
    max?: number;

    @definition("string", "optional")
    @affects("#label")
    @affects("#slots")
    @affects("#collection", "checkboxes")
    alias?: string;

    @definition("boolean", "optional")
    @affects("#slots")
    @affects("#collection", "checkboxes")
    exportable?: boolean;

    @definition("string", "optional")
    @affects("#collection", "checkboxes")
    labelForTrue?: string;

    @definition("string", "optional")
    @affects("#collection", "checkboxes")
    labelForFalse?: string;

    @definition("boolean", "optional")
    randomize?: boolean;

    @definition("string", "optional")
    @affects("#slots")
    @affects("#collection", "checkboxes")
    format?: "fields" | "concatenate" | "both";

    @definition("string", "optional")
    formatSeparator?:
        | "comma"
        | "space"
        | "list"
        | "bullets"
        | "numbers"
        | "conjunction"
        | "disjunction"
        | "custom";

    @definition("string", "optional")
    formatSeparatorCustom?: string;

    get label() {
        return npgettext(
            "block:checkboxes",
            "%2 (%1 checkbox)",
            "%2 (%1 checkboxes)",
            this.checkboxes.count,
            this.type.label
        );
    }

    @slots
    defineSlot(): void {
        this.slots.meta({
            type: Slots.Number,
            reference: "counter",
            label: pgettext("block:checkboxes", "Counter"),
            exportable: false,
            protected: true,
        });

        if (this.format === "concatenate" || this.format === "both") {
            this.slots.feature({
                type: Slots.Text,
                reference: "concatenation",
                label: pgettext("block:checkboxes", "Text value"),
                exportable: this.exportable,
                alias: this.alias,
                protected: true,
            });
        } else {
            this.slots.delete("concatenation", "feature");
        }
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        const collection = this.editor.collection({
            collection: this.checkboxes,
            title: pgettext("block:checkboxes", "Checkboxes"),
            icon: ICON_CHECKED,
            placeholder: pgettext("block:checkboxes", "Unnamed checkbox"),
            sorting: "manual",
            autoOpen: true,
            allowVariables: true,
            allowImport: true,
            allowExport: true,
            allowDedupe: true,
            showAliases: true,
            markdown:
                Markdown.MarkdownFeatures.Formatting |
                Markdown.MarkdownFeatures.Hyperlinks,
            indicator: (checkbox) =>
                (checkbox.exclusive &&
                    pgettext("block:checkboxes", "Exclusive").toUpperCase()) ||
                undefined,
            emptyMessage: pgettext(
                "block:checkboxes",
                "Click the + button to add a checkbox..."
            ),
        });

        this.editor.groups.settings();

        const min = new Forms.Numeric(
            Forms.Numeric.bind(this, "min", undefined)
        )
            .min(1)
            .max(this.max)
            .visible(isNumber(this.min))
            .indent(32)
            .width(75)
            .on(() => {
                max.min(this.min || 1);
            });
        const max = new Forms.Numeric(
            Forms.Numeric.bind(this, "max", undefined)
        )
            .min(this.min || 1)
            .visible(isNumber(this.max))
            .indent(32)
            .width(75)
            .on(() => {
                min.max(this.max);
            });

        this.editor.option({
            name: pgettext("block:checkboxes", "Limits"),
            form: {
                title: pgettext("block:checkboxes", "Limits"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:checkboxes",
                            "Minimum number of selected checkboxes"
                        ),
                        isNumber(this.min)
                    ).on((c) => {
                        min.visible(c.isChecked);
                    }),
                    min,
                    new Forms.Checkbox(
                        pgettext(
                            "block:checkboxes",
                            "Maximum number of selected checkboxes"
                        ),
                        isNumber(this.max)
                    ).on((c) => {
                        max.visible(c.isChecked);
                    }),
                    max,
                ],
            },
            activated: isNumber(this.max) || isNumber(this.min),
        });

        this.editor.option({
            name: pgettext("block:checkboxes", "Randomization"),
            form: {
                title: pgettext("block:checkboxes", "Randomization"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:checkboxes",
                            "Randomize the checkboxes (using [Fisher-Yates shuffle](%1))",
                            "https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle"
                        ),
                        Forms.Checkbox.bind(this, "randomize", undefined, true)
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.randomize),
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();

        const defaultLabelForTrue = pgettext("block:checkboxes", "Checked");
        const defaultLabelForFalse = pgettext(
            "block:checkboxes",
            "Not checked"
        );

        this.editor.option({
            name: pgettext("block:checkboxes", "Labels"),
            form: {
                title: pgettext("block:checkboxes", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForTrue", undefined)
                    ).placeholder(defaultLabelForTrue),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForFalse", undefined)
                    ).placeholder(defaultLabelForFalse),
                    new Forms.Static(
                        pgettext(
                            "block:checkboxes",
                            "These labels will be used in the dataset and override the default values %1 and %2.",
                            `**${defaultLabelForTrue}**`,
                            `**${defaultLabelForFalse}**`
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.labelForTrue) || isString(this.labelForFalse),
        });

        this.editor.scores({
            target: this,
            collection,
            description: pgettext(
                "block:checkboxes",
                "Generates a score based on the selected checkboxes. Open the settings panel for each checkbox to set the individual score for that checkbox."
            ),
        });

        const alias = this.editor
            .alias(this)
            .disabled(this.format !== "concatenate" && this.format !== "both");

        const formatSeparatorCustom = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "formatSeparatorCustom", undefined)
        )
            .visible(this.formatSeparator === "custom")
            .sanitize(false)
            .width(200)
            .label(pgettext("block:checkboxes", "Use this separator:"));

        const formatSeparatorOptions = new Forms.Group([
            new Forms.Dropdown<
                | "comma"
                | "space"
                | "list"
                | "bullets"
                | "numbers"
                | "conjunction"
                | "disjunction"
                | "custom"
            >(
                [
                    {
                        label: pgettext("block:checkboxes", "Comma separated"),
                        value: "comma",
                    },
                    {
                        label: pgettext("block:checkboxes", "Space separated"),
                        value: "space",
                    },
                    {
                        label: pgettext(
                            "block:checkboxes",
                            "List on multiple lines"
                        ),
                        value: "list",
                    },
                    {
                        label: pgettext("block:checkboxes", "Bulleted list"),
                        value: "bullets",
                    },
                    {
                        label: pgettext("block:checkboxes", "Numbered list"),
                        value: "numbers",
                    },
                    {
                        label: pgettext(
                            "block:checkboxes",
                            "Language sensitive conjunction (_, _, and _)"
                        ),
                        value: "conjunction",
                    },
                    {
                        label: pgettext(
                            "block:checkboxes",
                            "Language sensitive disjunction (_, _, or _)"
                        ),
                        value: "disjunction",
                    },
                    {
                        label: pgettext("block:checkboxes", "Custom separator"),
                        value: "custom",
                    },
                ],
                Forms.Radiobutton.bind(
                    this,
                    "formatSeparator",
                    undefined,
                    "comma"
                )
            )
                .label(
                    pgettext(
                        "block:checkboxes",
                        "How to separate the selected checkboxes:"
                    )
                )
                .on((formatSeparator) => {
                    formatSeparatorCustom.visible(
                        formatSeparator.value === "custom"
                    );
                }),
            formatSeparatorCustom,
        ]).visible(this.format === "concatenate" || this.format === "both");

        this.editor.option({
            name: pgettext("block:checkboxes", "Data format"),
            form: {
                title: pgettext("block:checkboxes", "Data format"),
                controls: [
                    new Forms.Radiobutton<"fields" | "concatenate" | "both">(
                        [
                            {
                                label: pgettext(
                                    "block:checkboxes",
                                    "Every checkbox as a separate field"
                                ),
                                description: pgettext(
                                    "block:checkboxes",
                                    "Every checkbox is included in the dataset as a separate value."
                                ),
                                value: "fields",
                            },
                            {
                                label: pgettext(
                                    "block:checkboxes",
                                    "Text field with a list of all selected checkboxes"
                                ),
                                description: pgettext(
                                    "block:checkboxes",
                                    "All the selected checkboxes are concatenated to a single string of text separated using a configurable separator."
                                ),
                                value: "concatenate",
                            },
                            {
                                label: pgettext(
                                    "block:checkboxes",
                                    "Both options above"
                                ),
                                description: pgettext(
                                    "block:checkboxes",
                                    "Includes every checkbox in the dataset together with the concatenated text."
                                ),
                                value: "both",
                            },
                        ],
                        Forms.Radiobutton.bind(
                            this,
                            "format",
                            undefined,
                            "fields"
                        )
                    )
                        .label(
                            pgettext(
                                "block:checkboxes",
                                "This setting determines how the data is stored in the dataset:"
                            )
                        )
                        .on((format) => {
                            formatSeparatorOptions.visible(
                                format.value === "concatenate" ||
                                    format.value === "both"
                            );

                            alias.disabled(
                                this.format !== "concatenate" &&
                                    this.format !== "both"
                            );
                        }),
                    formatSeparatorOptions,
                ],
            },
            activated: isString(this.format),
        });

        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        this.checkboxes.each((checkbox: Checkbox) => {
            if (checkbox.name) {
                this.conditions.template({
                    condition: CheckboxCondition,
                    markdown: checkbox.name,
                    icon: ICON_CHECKED,
                    burst: true,
                    props: {
                        slot: this.slots.select(checkbox.id),
                        checkbox,
                        checked: true,
                    },
                });
            }
        });

        if (this.checkboxes.count > 0) {
            this.conditions.template({
                condition: UncheckedCondition,
                separator: true,
            });
        }

        const counter = this.slots.select("counter", "meta");

        if (counter && counter.label) {
            const group = this.conditions.group(counter.label, ICON_COUNTER);

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:checkboxes",
                            "Counter is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:checkboxes",
                            "Counter is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:checkboxes",
                            "Counter is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:checkboxes",
                            "Counter is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext(
                            "block:checkboxes",
                            "Counter is between"
                        ),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:checkboxes",
                            "Counter is not between"
                        ),
                    },
                ],
                (condition: { mode: TCounterModes; label: string }) => {
                    group.template({
                        condition: CounterCondition,
                        label: condition.label,
                        autoOpen: true,
                        props: {
                            slot: counter,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(score.label, ICON_SCORE);

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:checkboxes",
                            "Score is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:checkboxes",
                            "Score is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:checkboxes",
                            "Score is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:checkboxes",
                            "Score is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext("block:checkboxes", "Score is between"),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:checkboxes",
                            "Score is not between"
                        ),
                    },
                    {
                        mode: "defined",
                        label: pgettext(
                            "block:checkboxes",
                            "Score is calculated"
                        ),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:checkboxes",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
